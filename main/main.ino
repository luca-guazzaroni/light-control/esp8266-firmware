// Include the WebServer library
#include <ESP8266WebServer.h>   
// Handle JSOn data
#include <ArduinoJson.h>

/**
 * Defines 
 */
// Light on/off status
#define ON true
#define OFF false
// Light Information
struct LightStatus {
  int level = 100;  // brightness
  bool state = OFF; // on/off
};
// Led on/off functions
#define LED_ON  digitalWrite(LED_PIN, LOW)
#define LED_OFF digitalWrite(LED_PIN, HIGH)

/**
 * Global variables
 */
// Led GPIO pin
const int LED_PIN = 2;
// Light status (state on/off and brightness level)
LightStatus Light;
// Access Point credentials
const char* ssid = "NodeMCU"; 
const char* password = "12345678";
// Access Point IP Address details */
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);
// Webserver object that listens for HTTP request on port 80
ESP8266WebServer server(80); 

/**
 * Functions Prototypes for HTTP handlers
 */
void handleRoot();      
void handleLightLevel();
void handleLightState();
void handleNotFound();

/**
 * Device configuration
 */
void setup(void){
  // Config LED's pin as a output
  pinMode(LED_PIN, OUTPUT);
  // Led starts turned off
  LED_OFF;
  // Config PWM range
  analogWriteRange(100);
  // Config ESP8266 as Access Point
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  // A little waiting
  delay(100);
  // Config url and handlers
  server.on("/", HTTP_GET, handleRoot);         // Call the 'handleRoot' function when a client requests URI "/" with GET method      
  server.on("/state/", HTTP_POST, handleState); // Call the 'handleState' function when a client requests URI "/state/ with POST method"       
  server.on("/level/", HTTP_POST, handleLevel); // Call the 'handleLevel' function when a client requests URI "/level/ with POST method"
  server.onNotFound(handleNotFound);            // When a client requests an unknown URI, call function "handleNotFound"
  // Actually start the server
  server.begin();                           
}

/*
 * Infinite loop
 */
void loop(void){
  // Listen for HTTP requests from clients
  server.handleClient();  
  // Control led status and pwm                  
  if(Light.state == ON){
    analogWrite(LED_PIN, 100-Light.level); // Led controlled with PWM
  } else {
    LED_OFF; // Led turned off
  }
}

/*
 * handleRoot()
 * returns JSON with LightStatus fields
 */
void handleRoot() {
  String response = ""; // return String
  DynamicJsonDocument json(256); // Json handler
  // make JSON object with LightStatus fields
  json["state"] = Light.state;
  json["level"] = Light.level;
  // Convert json object into String
  serializeJsonPretty(json, response);
  // Return data
  server.send(200, "application/json", response);
}

/**
 * handleLevel()
 * POST arg is "level", then set Lightstatus level
 */
void handleLevel(){
  DynamicJsonDocument doc(1024); 
  DeserializationError error = deserializeJson(doc, server.arg("plain"));
  // check if there is errors
  if (error)
    return;
  // set LightStatus level
  Light.level = doc["level"];
  // response
  server.sendHeader("Location", "/level/");
  server.send(200);
}

/**
 * handleState()
 * POST arg is "level", then set Lightstatus state
 */
void handleState(){
  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, server.arg("plain"));
  // check if there is errors
  if (error)
    return;
  // set LightStatus state
  Light.state = doc["state"];
  // response
  server.sendHeader("Location", "/state/");
  server.send(200);
}

/*
 * handleNotFound()
 * Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
 */
void handleNotFound(){
  String response = ""; // return String
  DynamicJsonDocument json(256); // Json handler
  // make JSON object with error msg
  json["error"] = "Not Found";
  // Convert json object into String
  serializeJsonPretty(json, response);
  // Response
  server.send(404, "application/json", response);
}
